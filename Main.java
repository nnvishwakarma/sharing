package firebasemessaging;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.TopicManagementResponse;

public class Main {
	
	public static void main(String ... args) throws FirebaseMessagingException, IOException {
		List<String> token = Arrays.asList("ANDROID");
		String topic = "test";
		FirebaseApp app = FirebaseApp.initializeApp();	
		System.out.println("App Name : "+app.getName());
		
		Message message = Message.builder()
			    .putData("score", "850")
			    .putData("time", "2:45")
			    .setTopic(topic)
			    .build();
		
		FirebaseMessaging firebaseMessaging = FirebaseMessaging.getInstance(app);
		String msgId = firebaseMessaging.send(message);
		System.out.println("Message Send : "+msgId);
		TopicManagementResponse response = firebaseMessaging.subscribeToTopic(token, topic);		
		response.getSuccessCount();
		System.out.println(response.getSuccessCount());	
		
	}

}
